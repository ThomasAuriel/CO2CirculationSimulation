
## Install conda at ~/.conda
conda create -n co2circulationsimulator python
conda activate co2circulationsimulator

# conda update --all --yes
# conda update -n base -c defaults conda

pip install scipy

pip install matplotlib

# dev
pip install mypy
pip install black
pip install autoflake
