# CO2 Ciriculation Simulation

## Model

![./documentation/Model.png]

## Hypothesis and limitations

- Perfect mix: When carbon is added to a stock, the mix is supposed to be perfect. When it is not the case, the stock is divided in two (like for ocean).
- Simple climat retroactions: The modernization of the climate retroactions is simplified. For instance, the soil respiration is supposed to be proportional to the carbon content and does not take into consideration temperature.
- No tipping points: The model equations don't change over time. This means that the reaction of the Earth system will be always the same. However, a tipping point is a specific point of the system when there is no easy come back due to a change in the exchange equations or a new system equilibrium. For instance, if the permafrost comes to melt, the flux of soil decay ($f_rs$) will increase to represent the discharge of carbon into the atmosphere. Another example is the complete loss of a forest. After some degradation, the forest will not be able to recover even if the temperature/moisture conditions are reversed. There is no more sampling to grow.
- No regional modulation: The model is global. Soil, ocean, plant reservoir are the reservoirs most subject to regional influence (due to chemistry environment and biological content). There reactions are different from an area to another (the desert will not discharge the same quantity carbon into the ocean than the Amazonia).
- No long cycle modulation: No natural climate cycles (El Nino, Schwabe, Hale, Gleissberg, Suess, etc) are modeled. If no human activities are modeled then model is stable and nothing happens.

All this limitations are to be kept in mind to use the model. However, for reasonable variations (not triggering tipping point) on a limited time (few centuries), the model is a good example of the Earth carbon cycle reaction to human activities.

## Sources

- Model
  https://www.sciencedirect.com/science/article/pii/S2405896322008862?via%3Dihub

- Our World in Data
  https://ourworldindata.org/fossil-fuels#global-fossil-fuel-consumption
  https://github.com/owid/co2-data

- The Global Carbon Project
  https://zenodo.org/record/7215364#.Y3y3sezMIeY