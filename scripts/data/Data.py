from pandas import DataFrame


class GenericData:
    name: str
    data: DataFrame

    filepath: str

    def __init__(self, name: str, filepath: str) -> None:
        self.name = name
        self.filepath = filepath

    def loadData(self):
        """
        Load data from file in filepath into self.data
        """

    def getData(self, time: int) -> float:
        """
        Get data at specific time

        Parameters
        ----------
        time: int
            Year of the data to retreive

        Return
        ------
        float
            Specific data for the time
        """
