import math
import numpy as np
from scipy.integrate import quad, solve_ivp
import matplotlib.pyplot as plt
from data.DataCO2 import DataCO2_ExtractionFossilFuel, DataCO2_LandUseCO2
import datetime

years = 500
now = datetime.datetime.now().year-1850
# https://www.sciencedirect.com/science/article/pii/S2405896322008862?via%3Dihub
# http://globecarboncycle.unh.edu/diagram.shtml
# http://globecarboncycle.unh.edu/CarbonPoolsFluxes.shtml

# Initial values
Ca_o = 560  # PgC - Today 750, before 1850 560
Cp_o = 560  # PgC
Cs_o = 1500  # PgC
Cso_o = 890  # PgC
Cdo_o = 38000  # PgC
Cr_o = 100e6  # PgC
Cw_o = 0  # PgC
Cf_o = 4000#746  # PgC - https://www.sciencedirect.com/science/article/pii/S0959378015300637
Av_o = 1  # Normalized

Ca = Ca_o  # PgC - Today 750, before 1850 560
Cp = Cp_o  # PgC
Cs = Cs_o  # PgC
Cso = Cso_o  # PgC
Cdo = Cdo_o  # PgC
Cr = Cr_o  # PgC
Cw = Cw_o  # PgC
Cf = Cf_o  # PgC
Av = Av_o  # Normalized

# Model settings
alpha = 2 / 3

# Constants
kl = 55  # PgC/y
kp = 110  # PgC/y
krp = 55  # PgC/y
krs = 55  # PgC/y
kt = 0.8  # PgC/y
kao = 0.278  # PgC/y ppm
kdw = 90.1  # PgC/y
kup = 90  # PgC/y
ks = 0.1  # PgC/y
AT = 2.222  # M - https://biocycle.atmos.colostate.edu/shiny/carbonate/

rv = 0.1  # PgC/y - Rate of volcanic eruption

kuf = 10  # PgC/y - Rate of fossil fuel combusion
ub = 1.5  # PgC/y - Rate of biommas combution
ul = 0.5  # PgC/y - Rate of harvess loss
up = 0.1  # PgC/y - Rate of product harvest
s = 0.0  # PgC/y - target rate of biomass production intended when planting seedlings

# Historical data
fossilFuelExtraction = DataCO2_ExtractionFossilFuel()
landuse_CO2 = DataCO2_LandUseCO2()
# Biomass combustion
# Harvess loss
# Rate of product harvest


def model(t, x):
    Ca, Cp, Cs, Cso, Cdo, Cr, Cw, Cf, Av = x
    # Ca : PgC - Mass of the carbon in the atmosphere
    # Cp : PgC - Mass of the carbon in plants
    # Cs : PgC - Mass of the carbon in soils
    # Cso : PgC - Mass of the carbon in surface ocean
    # Cdo : PgC - Mass of the carbon in deep ocean
    # Cr : PgC - Mass of the carbon in rocks
    # Av : 1 - Normalized vegetabled land area

    # Factors depending the Ca
    def nc(Ca):
        # Factor effect of CO2 on the photosynthesis
        return 1.5 * (pa(Ca) - 40) / (pa(Ca) + 80)

    def nT(Ca):
        # Factor effect of temperature on photosynthesis
        return (60 - Tg(Ca)) * (Tg(Ca) + 15) / 1350

    def pa(Ca):
        # Concentration of CO2 in atmosphere in ppm
        return 280 * Ca / Ca_o

    def Tg(Ca):
        # Global temperature
        return 15 + 0.01 * (pa(Ca) - 280)

    def Tg_sea(Ca):
        return Tg(Ca)

    # Water chimistry
    def KCO2(Ca):
        # CO2 equilibrium factor
        return 0.0255 + 0.0019 * Tg_sea(Ca)

    def KCO3(Ca):
        # CO3 equilibrium factor
        return 0.000545 + 0.000006 * Tg_sea(Ca)

    def HCO3(Ca, Cso):
        # HCO3 concentration in mM
        return (
            CO2(Cso) - math.sqrt(CO2(Cso) ** 2 - AT * (2 * CO2(Cso) - AT) * (1 - 4 * KCO3(Ca)))
        ) / (1 - 4 * KCO3(Ca))

    def CO2(Cso):
        # CO2 concentration in mM
        return Cso / (12 * 36.2)

    def CO3(AT, Ca, Cso):
        # CO3 concentration in mM
        return (AT - HCO3(Ca, Cso)) / 2

    def pa_ocean(Ca, Cso):
        return 280 * KCO2(Ca) * (HCO3(Ca, Cso) ** 2) / CO3(AT, Ca, Cso)

    def fp(Ca, Cp, Av):
        # PgC/y - Rate of photosynthesis
        return kp * nc(Ca) * nT(Ca) * (Cp / Cp_o) ** alpha * (Av / Av_o) ** (1 - alpha)

    def frp(Cp):
        # PgC/y - rate of plant respiration
        return krp * Cp / Cp_o

    def fl(Cp):
        # PgC/y - rate of litrerfall
        return kl * Cp / Cp_o

    def frs(Cs):
        # PgC/y - Rate of soil respiration
        return krs * Cs / Cs_o

    def ft(Cs):
        # PgC/y - Rate of transfer from soils to the surface ocean by rivers
        return kt * Cs / Cs_o

    def fa(Ca):
        # PgC/y - Net rate of absorption to the surface ocean
        return kao * (pa(Ca) - pa_ocean(Ca, Cso))

    def fdw(Cso):
        # PgC/y - Rate of down-welling from the surface ocean
        return kdw * Cso / Cso_o

    def fup(Cdo):
        # PgC/y - Rate of the up-weilling from the deep ocean
        return kup * Cdo / Cdo_o

    def fs(Cdo):
        # PgC/y - rate of sedimentation to the crust
        return ks * Cdo / Cdo_o

    # Human activities
    def uf():
        # Fossil fuel extraction
        return fossilFuelExtraction.getData(t) if t < now else 0
        # return kuf if t < now else -2

    def ub():
        return 0 # landuse_CO2.getData(t)  # if t < now else 1.5
        return 1.5 # landuse_CO2.getData(t)  # if t < now else 1.5

    # def q():
    #     # PgC/y - Rate of fossil fuel combustion
    #     return kuf + ub()
    def h():
        # PgC/y - Rate of plant harvesting
        return ub() + up + ul

    def fd(h, s, Cp, Av):
        # PgC/y - Net rate of deforestation
        return 0
        p = 1 / (1 - alpha)
        k = kp / (p * Cp_o)
        g = lambda x: s * (1 - math.exp(-1 * k * x)) ** p
        return (h() - quad(g, 0, t)[0]) * Av / Cp

    dCa = frp(Cp) + frs(Cs) - fp(Ca, Cp, Av) - fa(Ca) + rv + uf() + ub()
    dCp = fp(Ca, Cp, Av) - frp(Cp) - fl(Cp) - h()
    dCs = fl(Cp) - frs(Cs) - ft(Cs) + ul
    dCso = fa(Ca) + ft(Cs) + fup(Cdo) - fdw(Cso)
    dCdo = fdw(Cso) - fup(Cdo) - fs(Cdo)
    dCr = fs(Cdo) - rv
    dCw = up
    dCf = -uf()
    dA = -fd(h, s, Cp, Av)

    # # Debug
    # dCa  = - fp(Ca, Cp, Av) - fa(Ca)
    # dCp  = fp(Ca, Cp, Av)-frp(Cp)-fl(Cp)
    # dCs  = fl(Cp)-frs(Cs)-ft(Cp)
    # dCr  = 0
    # dCso = fa(Ca) + ft(Cp) + fup(Cdo) - fdw(Cso)
    # dCdo = fdw(Cso) - fup(Cdo) - fs(Cdo)
    # dCw  = 0
    # dCf  = 0
    # dA = 0

    return [dCa, dCp, dCs, dCso, dCdo, dCr, dCw, dCf, dA]


t = np.linspace(0, years)
ret = solve_ivp(
    model,
    t_span=[0, years],
    y0=[Ca, Cp, Cs, Cso, Cdo, Cr, Cw, Cf, Av],
    printmessg=True,
    method="LSODA",
    max_step=0.1,
)
Ca, Cp, Cs, Cso, Cdo, Cr, Cw, Cf, Av = ret.y
dCa, dCp, dCs, dCso, dCdo, dCr, dCw, dCf, dAv = np.diff(ret.y)
t = ret.t


def pa(Ca):
    # Concentration of CO2 in atmosphere in ppm
    return 280 * Ca / Ca_o


def Tg(Ca):
    # Global temperature
    return 15 + 0.01 * (pa(Ca) - 280)


temp = Tg(Ca)
ppm = pa(Ca)

# CO2 stocks
fig = plt.figure()
ax = fig.add_subplot(231)
ax.plot(t, Ca, alpha=0.5, label="Ca - Carbon in the atmospher", c="C0")
ax.plot(t, Cp, alpha=0.5, label="Cp - Carbon in plants", c="C1")
ax.plot(t, Cs, alpha=0.5, label="Cs - Carbon in soil", c="C2")
# ax.plot(t, Cr, alpha=0.5, label='Cr - Carbon in lithosphere', c='C3')
ax.plot(t, Cso, alpha=0.5, label="Cso - Carbon in surface ocean", c="C4")
ax.plot(t, Cdo, alpha=0.5, label="Cdo - Carbon in deap ocean", c='C5')
ax.plot(t, Cw, alpha=0.5, label="Cw - Carbon in products", c="C6")
ax.plot(t, Cf, alpha=0.5, label="Cf - Carbon in fossils", c="C7")

ax.set_title("Absolute evolution of carbon stocks")
ax.set_xlabel("Years")
ax.set_ylabel("Carbon stock [PgC]")
ax.grid()
# legend = ax.legend()

# Relative evolution of the stock
ax = fig.add_subplot(232)
ax.plot(t, Ca / Ca_o, alpha=0.5, label="Ca - Carbon in the atmospher", c="C0")
ax.plot(t, Cp / Cp_o, alpha=0.5, label="Cp - Carbon in plants", c="C1")
ax.plot(t, Cs / Cs_o, alpha=0.5, label="Cs - Carbon in soil", c="C2")
# ax.plot(t, Cr/Cr_o, alpha=0.5, label='Cr - Carbon in lithosphere', c='C3')
ax.plot(t, Cso / Cso_o, alpha=0.5, label="Cso - Carbon in surface ocean", c="C4")
ax.plot(t, Cdo / Cdo_o, alpha=0.5, label="Cdo - Carbon in deap ocean", c='C5')
ax.plot(t, Cw/Cw_o, alpha=0.5, label='Cw - Carbon in products', c='C6')
ax.plot(t, Cf / Cf_o, alpha=0.5, label="Cf - Carbon in fossils", c="C7")

ax.set_title("Relative evolution of carbon stocks")
ax.set_xlabel("Years")
ax.set_ylabel("Relative evolution of carbon [Ratio]")
ax.grid()
# legend = ax.legend()

# Flux evolution
ax = fig.add_subplot(233)
ax.plot(t[:-1], dCa, alpha=0.5, label="Ca - Carbon in the atmospher", c="C0")
ax.plot(t[:-1], dCp, alpha=0.5, label="Cp - Carbon in plants", c="C1")
ax.plot(t[:-1], dCs, alpha=0.5, label="Cs - Carbon in soil", c="C2")
# ax.plot(t[:-1], dCr, alpha=0.5, label='Cr - Carbon in lithosphere', c='C3')
ax.plot(t[:-1], dCso, alpha=0.5, label="Cso - Carbon in surface ocean", c="C4")
ax.plot(t[:-1], dCdo, alpha=0.5, label="Cdo - Carbon in deap ocean", c='C5')
ax.plot(t[:-1], dCw, alpha=0.5, label='Cw - Carbon in products', c='C6')
ax.plot(t[:-1], dCf, alpha=0.5, label="Cf - Carbon in fossils", c="C7")

ax.set_title("Derivate evolution of carbon concentration")
ax.set_xlabel("Years")
ax.set_ylabel("Derivate carbon evolution [PgC/y]")
ax.grid()
legend = ax.legend()

# Temperature
ax = fig.add_subplot(234)

ax.plot(t, temp, alpha=0.5, label="T - Surface temperature")

ax.set_title("Temperature Evolution")
ax.set_xlabel("Years")
ax.set_ylabel("[°C]")
ax.grid()
legend = ax.legend()

# CO2 Concentration
ax = fig.add_subplot(235)

ax.plot(t, ppm, alpha=0.5, label="Pa - Atmospheric carbon")

ax.set_title("Atmospheric carbon concentration")
ax.set_xlabel("Years")
ax.set_ylabel("ppm")
ax.grid()
legend = ax.legend()


plt.show()

print("end")
