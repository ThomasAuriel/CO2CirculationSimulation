from pandas import DataFrame
import pandas
from data.Data import GenericData
import numpy


class DataCO2_ExtractionFossilFuel(GenericData):
    name: str
    data: DataFrame

    filepath: str

    def __init__(
        self, name: str = "Atmospheric CO2", filepath: str = "./input/GCB2022v27_MtCO2_flat.csv"
    ) -> None:
        super().__init__(name=name, filepath=filepath)
        self.loadData()

    def loadData(self):
        """
        Load data from file in filepath
        """

        df = pandas.read_csv(filepath_or_buffer=self.filepath).fillna(0)
        df = df.loc[:, df.columns.isin(["Country", "Year", "Total"])]

        df = df.set_index(["Country", "Year"])
        df = df.loc[
            (df.index.get_level_values(1) >= 1850), :
        ]
        df = df.groupby(["Year"]).sum()
        df = df.reset_index()
        df = df.loc[:, df.columns.isin(["Total"])]

        ## Change unit : MtCO2 -> GtC02 -> PgC
        df = df / 1e3 * 0.27

        self.data = df

    def getData(self, time: int) -> float:
        """
        Get data at specific time

        Parameters
        ----------
        time: int
            Year of the data to retreive

        Return
        ------
        float
            Specific data for the time
        """
        index = numpy.abs(self.data.index.values - time).argmin()
        return self.data.iloc[index, 0]


class DataCO2_LandUseCO2(GenericData):
    name: str
    data: DataFrame

    filepath: str

    def __init__(
        self, name: str = "Land-use CO2", filepath: str = "./input/owid-co2-data.csv"
    ) -> None:
        super().__init__(name=name, filepath=filepath)
        self.loadData()

    def loadData(self):
        """
        Load data from file in filepath
        """

        df = pandas.read_csv(filepath_or_buffer=self.filepath).fillna(0)
        df = df.loc[:, df.columns.isin(["country", "year", "land_use_change_co2"])]

        df = df.set_index(["country", "year"])
        df = df.loc[
            (df.index.get_level_values(0) == "World") & (df.index.get_level_values(1) >= 1850), :
        ]
        df = df.reset_index()

        df = df.drop(labels=["country", "year"], axis=1)

        # Change unit : MtC -> GtC=PgC
        df = df / 1e3

        self.data = df

    def getData(self, time: int) -> float:
        """
        Get data at specific time

        Parameters
        ----------
        time: int
            Year of the data to retreive

        Return
        ------
        float
            Specific data for the time
        """
        index = numpy.abs(self.data.index.values - 15).argmin()
        return self.data.iloc[index, 0]
